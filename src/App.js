import './App.css';
import Routers from './component/router';

function App() {
  return (
    <div className="App">
      <Routers />
    </div>
  );
}

export default App;
