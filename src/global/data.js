const uuid = require('uuid/dist/v4')
let data = [
    {
        productId: uuid(),
        image: '',
        name: '',
        sellProduct: 0,
        buyProduct: 0,
        stock: 0
    }
]

const dataNoPagi = () => {
    const response = {
        data
    }
    return response
}

const dataWithPagi = (payload) => {
    const { page } = payload

    const meta = {
        page: page,
        count: data.length,
        totalPage: Math.ceil(data.length / limit),
        totalData: data.length
    };
    const response = {
        data,
        meta
    }
    return response
}

exports.default = {
    dataNoPagi,
    dataWithPagi
}