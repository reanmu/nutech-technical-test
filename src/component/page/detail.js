import React, { useEffect, useState} from "react";
import { useParams } from "react-router-dom";
import axios from 'axios';
import urls from "../../global/config"
import '../css/main.css';

const Details = () => {
    const { id } = useParams();
    let [product, setData] = useState({ data:[] });
    const [query, setQuery] = useState(id);
    useEffect(() => {
        const fetchData = async () => {
        const result = await axios.get(`${urls}/product/${query}`);
        // console.log(result);
        setData(result.data);
    };
    fetchData();
    }, []);

    return (
        <div className="main">
            <div className="main-page">
                { 
                        <div className="card shadow-lg p-3 mb-5 bg-body">
                            <img src={(product.data.image)} className="card-img-top" alt={(product.data.name)}></img>
                            <div className="card-body">
                                <h5 className="card-title">{(product.data.name)}</h5>
                                <p className="card-text">IDR {(product.data.sellProduct)}</p>
                            </div>
                        </div>
                }
                <div className="main main-details card-details shadow-lg p-3 mb-5 bg-body">
                    {
                            <div>
                                <div className="cars-details">
                                    <h4 className="card-details-title">ID {(product.data.productId)} - {(product.data.name)} - {(i.strCategory)} - {(i.strArea)}</h4>
                                    <h6 className="card-details-title">Ingredients :</h6>
                                    <p className="card-details-text">
                                    </p>
                                    <h6 className="card-details-title">Instructions :</h6>
                                    <p className="card-details-text">{(i.strInstructions)}</p>
                                    <h6 className="card-details-title">Another Reference :</h6>
                                    <a className="card-details-text-a" href={(i.strYoutube)}>Watch Here ~</a>
                                    <br></br><br></br><br></br>
                                    <h6 className="card-details-title">Tags :</h6>
                                    <p className="card-details-text">{(i.strTags)}</p>
                                </div>
                            </div>
                    }
                </div>
            </div>
        </div>
    )
}

export default Details;