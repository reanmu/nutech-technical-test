import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom'
import axios from 'axios';
import urls from '../../global/config'
import '../css/bootstrap.min.css'
import '../css/main.css'


const Main = () => {
    let [product, setData] = useState({ data: [] });
    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get(`${urls}/product`);
            setData(result.data);
        };
        fetchData();
    }, []);

    return (
        <div className="main">
            <div className="main-page">
                {
                    product.data.map((i) => (
                        <div className="card shadow-lg p-3 mb-5 bg-body">
                            <img src={(i.image)} className="card-img-top" alt={(i.name)}></img>
                            <div className="card-body">
                                <h5 className="card-title">{(i.name)}</h5>
                                <p className="card-text">IDR {(i.sellProduct)}</p>
                                <Link to={{ pathname: `/detail/${i.productId}` }}
                                    className="btn btn-primary">View Details</Link>
                            </div>
                        </div>
                    ))
                }
            </div>
        </div>
    )
}

export default Main;
